//Nombre del proyecto
name := "Lyapunov fractal generator"

version := "0.1"

organization := "com.ciriscr"

scalaVersion := "2.10.2"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases"

//Casbah, Scalatest, akka, salat
libraryDependencies ++= Seq(
  "org.scalatest" % "scalatest_2.10" % "1.9.1" % "test",
  "org.scalala" % "scalala_2.9.1" % "1.0.0.RC2"
)

// para compilar en varias versiones de scala
crossScalaVersions := Seq("2.9.2", "2.10.0", "2.10.2")

// reduce the maximum number of errors shown by the Scala compiler
maxErrors := 30

// increase the time between polling for file changes when using continuous execution
pollInterval := 1000

// append several options to the list of options passed to the Java compiler
javacOptions ++= Seq("-source", "1.6", "-target", "1.6")

// append -deprecation to the options passed to the Scala compiler
scalacOptions ++= Seq("-deprecation", "-optimise", "-explaintypes")


// set the main class for packaging the main jar
// 'run' will still auto-detect and prompt
// change Compile to Test to set it for the test jar
//mainClass in (Compile, packageBin) := Some("myproject.MyMain")

// set the main class for the main 'run' task
// change Compile to Test to set it for 'test:run'
//mainClass in (Compile, run) := Some("myproject.MyMain")


// only use a single thread for building
parallelExecution := true
